//
//  StudentDriver.m
//  DelegationDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "StudentDriver.h"

@implementation StudentDriver

- (void)startDriving
{
    NSLog(@"Stall a few times, then jerkily pull out, leaving rubber");
}

@end
