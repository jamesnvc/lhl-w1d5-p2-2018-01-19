//
//  Car.m
//  DelegationDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Car.h"

@implementation Car

- (void)drive
{
    NSLog(@"Starting to drive");
    [self.driver startDriving];

    if ([self.driver respondsToSelector:@selector(canMakeASharpTurn)]) {
        NSLog(@"Oops, about to miss an exit!");
        if ([self.driver canMakeASharpTurn]) {
            NSLog(@"Screech across the road on two tires & make it");
        } else {
            NSLog(@"Oh well, we'll just be late");
        }
    }
}

@end
