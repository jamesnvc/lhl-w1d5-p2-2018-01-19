//
//  main.m
//  DelegationDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "ProDriver.h"
#import "StudentDriver.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        ProDriver *proDriver = [[ProDriver alloc] init];
        StudentDriver *student = [[StudentDriver alloc] init];

        Car* c1 = [[Car alloc] init];
        c1.driver = proDriver;
        [c1 drive];


        c1.driver = student;
        [c1 drive];
    }
    return 0;
}
