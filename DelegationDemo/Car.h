//
//  Car.h
//  DelegationDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CarDriver <NSObject>

@required

- (void)startDriving;

@optional

- (BOOL)canMakeASharpTurn;

@end

@interface Car : NSObject

@property (nonatomic,weak) id<CarDriver> driver;

- (void)drive;

@end
