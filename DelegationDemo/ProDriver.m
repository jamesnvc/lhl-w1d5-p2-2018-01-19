//
//  ProDriver.m
//  DelegationDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ProDriver.h"

@implementation ProDriver

- (void)startDriving
{
    NSLog(@"Smoothly pull out & start going");
}

- (BOOL)canMakeASharpTurn
{
    return YES;
}

@end
